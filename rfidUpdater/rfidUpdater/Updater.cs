﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Microsoft.Win32;

namespace rfidUpdater
{
    public partial class Updater : Form
    {
        private System.Timers.Timer timer;

        public delegate void deleg(String data);
        public delegate void deleg2(Int32 count);
        public delegate void deleg3(Object obj);

        private Int32 gecenZaman = 0;

        private Boolean kontrolBasladi = false;
        private Boolean updateVar = false;
        private Boolean updateBasladi = false;
        private Boolean indirmeBasarili = false;
        private Boolean dosyaKopyalandi = false;
        private Boolean programHazir = false;
        private Boolean DB_OK = false;

        public Updater()
        {
            InitializeComponent();
        }

        private void Updater_Load(object sender, EventArgs e)
        {
            ProcessModule objCurrentModule = Process.GetCurrentProcess().MainModule;
            objKeyboardProcess = new LowLevelKeyboardProc(captureKey);
            ptrHook = SetWindowsHookEx(13, objKeyboardProcess, GetModuleHandle(objCurrentModule.ModuleName), 0);
            
            timer = new System.Timers.Timer();
            timer.Interval = 1000;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_Elapsed);
            timer.Start();

            Logger.OnMessageChanged += new EventHandler<LogArgs>(OnMessageChanged);
                        
            lblBilgi.Text = "Ayarlar okunuyor..";

            while (!DB.InitDB())
            {
                DB_OK = false;
                Application.DoEvents();
            }

            DB_OK = true;
        }
         
        private void OnMessageChanged(Object obj, LogArgs logArgs)
        {
            DB.LogKaydiYap();
        }

        private void timer_Elapsed(object sender, EventArgs e)
        {
            gecenZaman++;
            if (DB_OK)
            {
                this.BeginInvoke(new deleg(Log), "Güncellemeler kontrol ediliyor..");

                if ((gecenZaman > 1) && !kontrolBasladi)
                {
                    updateKontrol();
                }

                if (updateVar && !updateBasladi)
                {
                    Thread updateThread = new Thread(new ThreadStart(UpdateYap));
                    updateThread.Start();
                    this.BeginInvoke(new deleg(Log), "Güncellemeler indiriliyor..");
                }

                if (updateVar && updateBasladi)
                {
                    this.BeginInvoke(new deleg(Log), "Güncellemeler indiriliyor..");
                }

                if (indirmeBasarili && !dosyaKopyalandi)
                {
                    this.BeginInvoke(new deleg(Log), "Dosyalar kopyalanıyor..");                    
                 }

                if (indirmeBasarili && dosyaKopyalandi)
                {
                    this.BeginInvoke(new deleg(Log), "Sistem açılıyor..");                    
                    Logger.isError = false;
                    Logger.Method = "";
                    Logger.Message = "güncelleme başarılı. yeni versiyon : " + Settings.NewVersion;
                    programHazir = true;
                }
                
                if (!updateVar && gecenZaman > Settings.UpdateTimeout)
                {
                    this.BeginInvoke(new deleg(Log), "Sistem açılıyor..");                    
                    Logger.isError = true;
                    Logger.Method = "updater, updateKontrol";
                    Logger.Message = "update kontrolü zaman aşımına uğradı";
                    programHazir = true;
                }

                if (programHazir)
                {
                    timer.Stop();
                    ProgramAc();
                }
            }
        }

        private void updateKontrol()
        {
            try
            {
                //FileVersionInfo version = FileVersionInfo.GetVersionInfo(Settings.StartPath);      
                kontrolBasladi = true;
                Version version;
                if (File.Exists(Settings.StartPath))
                {
                    version = AssemblyName.GetAssemblyName(Settings.StartPath).Version;
                }
                else
                {
                    version = new Version("1.0.0.0");
                }                
                
                String[] sversion = version.ToString().Split(Convert.ToChar("."));

                String strVersion = "";
                for (int i = 0; i < sversion.Length; i++)
                {
                    strVersion += sversion[i];
                }

                WebClient client = new WebClient();
                Stream stream = client.OpenRead(Settings.UpdateUrl + strVersion);                
                StreamReader reader = new StreamReader(stream);

                String content = reader.ReadToEnd();
                if (content.Length > 2)
                {
                    String[] contents = content.Split(Convert.ToChar(","));
                    if (contents[0] == "UPDATE")
                    {
                        updateVar = true;
                        Settings.NewVersion = contents[2];
                        Settings.FileUrl = contents[3];
                    }
                }
                else
                {
                    updateVar = false;
                }
            }
            catch (Exception ex)
            {
                updateVar = false;
                timer.Stop();
                this.BeginInvoke(new deleg(Log), "Güncelleme hatası!");
                Logger.isError = true;
                Logger.Method = "updater, updateKontrol";
                Logger.Message = ex.Message;                
            }
        }

        private void UpdateYap()
        {
            try
            {
                updateBasladi = true;

                WebClient webClient = new WebClient();
                webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(updateCompleted);
                webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(progressChanged);
                
                webClient.DownloadFileAsync(new Uri(Settings.FileUrl), Settings.DownloadPath);
                
                /*
                while (true)
                {
                    progressBar.Value++;
                    Application.DoEvents();
                }
                */
            }
            catch (Exception ex)
            {
                indirmeBasarili = false;
                timer.Stop();
                this.BeginInvoke(new deleg(Log), "Güncelleme hatası!");
                Logger.isError = true;
                Logger.Method = "updater, UpdateYap";
                Logger.Message = ex.Message; 
            }
        }

        private void DosyaKopyala()
        {
            try
            {
                File.Copy(Settings.DownloadPath, Settings.StartPath, true);
                dosyaKopyalandi = File.Exists(Settings.StartPath);
            }
            catch (Exception ex)
            {
                dosyaKopyalandi = false;
                timer.Stop();
                this.BeginInvoke(new deleg(Log), "Kopyalama hatası!");
                Logger.isError = true;
                Logger.Method = "updater, DosyaKopyala";
                Logger.Message = ex.Message;
            }
        }

        private void ProgramAc()
        {
            Process proc = new Process();
            proc.StartInfo.FileName = Settings.StartPath;
            if (proc.Start()) this.BeginInvoke(new deleg3(Kapat), this); 
        }

        private void Kapat(Object obj)
        {
            Form form = new Form();
            form = (Form)obj;
            form.Close();
        }

        private void Log(String logData)
        {
            lblBilgi.Text = logData;
        }

        private void Progress(Int32 count)
        {
            progressBar.Value = count;
            /*
            if (count > 100)
            {
                Process proc = new Process();
                proc.StartInfo.FileName = Settings.StartPath;
                if (proc.Start()) this.Close();
            }
            else
            {
                progressBar.Value = count;
            }
            */
        }

        private void progressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            this.BeginInvoke(new deleg2(Progress), e.ProgressPercentage);
            //progressBar.Value = e.ProgressPercentage;
        }

        private void updateCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Logger.isError = true;
                Logger.Method = "updater, updateCompleted";
                Logger.Message = e.Error.Message;
            }
            else
            {
                indirmeBasarili = true;
                DosyaKopyala();
            }
        }


    
        //***************************************************************************************************************
        //********************************************************************************************
        /* Code to Disable WinKey, Alt+Tab, Ctrl+Esc Starts Here */


        // Structure contain information about low-level keyboard input event 
        [StructLayout(LayoutKind.Sequential)]
        private struct KBDLLHOOKSTRUCT
        {
            public Keys key;
            public int scanCode;
            public int flags;
            public int time;
            public IntPtr extra;
        }
        //System level functions to be used for hook and unhook keyboard input  
        private delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int id, LowLevelKeyboardProc callback, IntPtr hMod, uint dwThreadId);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool UnhookWindowsHookEx(IntPtr hook);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hook, int nCode, IntPtr wp, IntPtr lp);
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string name);
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern short GetAsyncKeyState(Keys key);
        //Declaring Global objects     
        private IntPtr ptrHook;
        private LowLevelKeyboardProc objKeyboardProcess;

        private IntPtr captureKey(int nCode, IntPtr wp, IntPtr lp)
        {
            if (nCode >= 0)
            {
                KBDLLHOOKSTRUCT objKeyInfo = (KBDLLHOOKSTRUCT)Marshal.PtrToStructure(lp, typeof(KBDLLHOOKSTRUCT));
                
                // Disabling Windows keys 

                if (objKeyInfo.key == Keys.RWin || objKeyInfo.key == Keys.LWin || 
                    objKeyInfo.key == Keys.Tab && HasAltModifier(objKeyInfo.flags) || 
                    objKeyInfo.key == Keys.Escape && (ModifierKeys & Keys.Control) == Keys.Control ||
                    objKeyInfo.key == Keys.F4 && HasAltModifier(objKeyInfo.flags))
                {
                    return (IntPtr)1; // if 0 is returned then All the above keys will be enabled
                }
            }
            return CallNextHookEx(ptrHook, nCode, wp, lp);
        }

        bool HasAltModifier(int flags)
        {
            return (flags & 0x20) == 0x20;
        }

         /* Code to Disable WinKey, Alt+Tab, Ctrl+Esc Ends Here */

        /*
        private void SystemLock()
        {
            
            //KillCtrlAltDelete();
        }

        public void KillCtrlAltDelete()
        {
            RegistryKey regkey;
            string keyValueInt = "1";
            string subKey = @"Software\Microsoft\Windows\CurrentVersion\Policies\System";

            try
            {
                regkey = Registry.CurrentUser.CreateSubKey(subKey);
                regkey.SetValue("DisableTaskMgr", keyValueInt);
                regkey.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private delegate int LowLevelKeyboardProcDelegate(int nCode, int
            wParam, ref KBDLLHOOKSTRUCT lParam);

        [DllImport("user32.dll", EntryPoint = "SetWindowsHookExA", CharSet = CharSet.Ansi)]
        private static extern int SetWindowsHookEx(
           int idHook,
           LowLevelKeyboardProcDelegate lpfn,
           int hMod,
           int dwThreadId);

        [DllImport("user32.dll")]
        private static extern int UnhookWindowsHookEx(int hHook);


        [DllImport("user32.dll", EntryPoint = "CallNextHookEx", CharSet = CharSet.Ansi)]
        private static extern int CallNextHookEx(
            int hHook, int nCode,
            int wParam, ref KBDLLHOOKSTRUCT lParam);


        const int WH_KEYBOARD_LL = 13;
        private int intLLKey;
        private KBDLLHOOKSTRUCT lParam;


        private struct KBDLLHOOKSTRUCT
        {
            public int vkCode;
            int scanCode;
            public int flags;
            int time;
            int dwExtraInfo;
        }

        private int LowLevelKeyboardProc(
            int nCode, int wParam,
            ref KBDLLHOOKSTRUCT lParam)
        {
            bool blnEat = false;
            switch (wParam)
            {
                case 256:
                case 257:
                case 260:
                case 261:
                    //Alt+Tab, Alt+Esc, Ctrl+Esc, Windows Key
                    if (((lParam.vkCode == 9) && (lParam.flags == 32)) ||
                    ((lParam.vkCode == 27) && (lParam.flags == 32)) || ((lParam.vkCode ==
                    27) && (lParam.flags == 0)) || ((lParam.vkCode == 91) && (lParam.flags
                    == 1)) || ((lParam.vkCode == 92) && (lParam.flags == 1)) || ((true) &&
                    (lParam.flags == 32)))
                    {
                        blnEat = true;
                    }
                    break;
            }

            if (blnEat)
                return 1;
            else return CallNextHookEx(0, nCode, wParam, ref lParam);

        }

        private void KeyboardHook(object sender, EventArgs e)
        {
            intLLKey =
                SetWindowsHookEx(

                WH_KEYBOARD_LL,

                new LowLevelKeyboardProcDelegate(LowLevelKeyboardProc),

                System.Runtime.InteropServices.Marshal.GetHINSTANCE(
                  System.Reflection.Assembly.GetExecutingAssembly().GetModules()[0]).ToInt32(), 0);
        }

        private void ReleaseKeyboardHook()
        {
            intLLKey = UnhookWindowsHookEx(intLLKey);
        }
         */
    }
}
