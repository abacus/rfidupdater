﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rfidUpdater
{
    public static class Settings
    {
        private static Boolean admin;
        //private static String connStr = "Server=127.0.0.1;Database=TURNIKE_CLIENT;Trusted_Connection=True;";
        private static String connStr = "Server=10.5.5.56;Database=TURNIKE_CLIENT;User Id=sa;Password=147852369";
        private static String updUrl = "";
        private static Int32 updTimeout = 0;
        private static String fUrl = "";
        private static String dlPath = @"D:\Projecto\rfidupdater\rfidUpdater\rfidUpdater\bin\Debug\Rfidclient.exe";
        private static String startPath = @"D:\Projecto\rfidclient\rfidClient\rfidClient\bin\Debug\rfidClient.exe";
        private static String newVersion = "";

        public static Boolean AdminMod
        {
            get
            {
                return admin;
            }
            set
            {
                admin = value;
            }
        }

        public static String ConnectionStr
        {
            get
            {
                return connStr;
            }
            set
            {
                connStr = value;
            }
        }

        public static String UpdateUrl
        {
            get
            {
                return updUrl;
            }
            set
            {
                updUrl = value;
            }
        }

        public static Int32 UpdateTimeout
        {
            get
            {
                return updTimeout;
            }
            set
            {
                updTimeout = value;
            }
        }

        public static String FileUrl
        {
            get
            {
                return fUrl;
            }
            set
            {
                fUrl = value;
            }
        }
        
        public static String DownloadPath
        {
            get
            {
                return dlPath;
            }
            set
            {
                dlPath = value;
            }
        }

        public static String StartPath
        {
            get
            {
                return startPath;
            }
            set
            {
                startPath = value;
            }
        }

        public static String NewVersion
        {
            get
            {
                return newVersion;
            }
            set
            {
                newVersion = value;
            }
        }
    }
}
