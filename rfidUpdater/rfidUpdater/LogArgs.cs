﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rfidUpdater
{
    public class LogArgs : EventArgs
    {
        private String msg;
        private String meth;
        private Boolean isErr;

        public LogArgs(String message, String method, Boolean isError)
        {
            this.msg = message;
            this.meth = method;
            this.isErr = isError;
        }

        public string Message
        {
            get
            {
                return msg;
            }
        }

        public string Method
        {
            get
            {
                return meth;
            }
        }

        public Boolean isError
        {
            get
            {
                return isErr;
            }
        }
    }
}
