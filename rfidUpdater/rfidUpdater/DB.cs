﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;

namespace rfidUpdater
{
    public static class DB
    {
        private static SqlConnection sqlConn;
  
        public static Boolean InitDB()
        {
            Boolean sonuc;
            try
            {
                sqlConn = new SqlConnection(Settings.ConnectionStr);
                AyarlarOku();
                sonuc = true;
            }
            catch (Exception ex)
            {
                sonuc = false;
            }
            return sonuc;      
        }

        private static void AyarlarOku()
        {
            try
            {
                String sqlStr = "SELECT * FROM AYARLAR";
                SqlCommand sqlComm = new SqlCommand(sqlStr, sqlConn);
                if (sqlConn.State != System.Data.ConnectionState.Open) sqlConn.Open();
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlComm);
                DataTable dt = new DataTable();
                sqlAdapter.Fill(dt);

                Settings.UpdateUrl = Convert.ToString(dt.Rows[0]["UPDATE_URL"]);
                Settings.UpdateTimeout = Convert.ToInt32(dt.Rows[0]["UPDATE_TIMEOUT"]);
                Settings.DownloadPath = Convert.ToString(dt.Rows[0]["DL_PATH"]);
                Settings.StartPath = Convert.ToString(dt.Rows[0]["START_PATH"]);
                //return dt;
            }
            catch (Exception ex)
            {
                if (Settings.AdminMod)
                {
                    //Info.ErrorDesc = "Ayarlar Veritabanından Okunamadı! | " + ex.Message;
                }
                else
                {
                    //Info.ErrorDesc = "Ayarlar Veritabanından Okunamadı!";
                }                
                //return null;                
            }
        }

        public static void LogKaydiYap()
        {
            Thread logThread = new Thread(new ThreadStart(Logla));
            logThread.Start();
        }

        private static void Logla()
        {
            try
            {
                String sqlStr = "INSERT INTO LOG (ACIKLAMA, METHOD, KAYITID, HATA) VALUES(@ACIKLAMA, @METHOD, @KAYITID, @HATA)";
                SqlCommand sqlComm = new SqlCommand(sqlStr, sqlConn);
                sqlComm.Parameters.AddWithValue("ACIKLAMA", Logger.Message);
                sqlComm.Parameters.AddWithValue("METHOD", Logger.Method);
                sqlComm.Parameters.AddWithValue("KAYITID", 0);
                sqlComm.Parameters.AddWithValue("HATA", Logger.isError);             

                if (sqlConn.State != System.Data.ConnectionState.Open) sqlConn.Open();

                Int32 inserted = sqlComm.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                if (Settings.AdminMod)
                {
                    //Info.ErrorDesc = "Ayarlar Veritabanından Okunamadı! | " + ex.Message;
                }
                else
                {
                    //Info.ErrorDesc = "Ayarlar Veritabanından Okunamadı!";
                }
                //return null;                
            }
        }
    }
}
